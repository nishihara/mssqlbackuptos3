﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Topshelf.ServiceConfigurators;

namespace MSSQLBackupToS3
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
                {
                    x.Service<BackupService>(ConfigureService);
                    x.RunAsLocalSystem();
                    x.SetDescription("MSSQL database backup to AWS S3 service.");
                    x.SetDisplayName("MSSQL backup to S3");
                    x.SetServiceName("MSSQLBackupToS3");
                    x.EnableServiceRecovery(rc =>
                        {
                            rc.RestartService(2); 
                        });
                    x.StartAutomatically();
                });
        }

        private static void ConfigureService(ServiceConfigurator<BackupService> sc)
        {
            sc.ConstructUsing(() => new BackupService(
                MSSQLBackupToS3.Properties.Settings.Default.AWSAccessKey, MSSQLBackupToS3.Properties.Settings.Default.AWSSecretAccessKey,
                MSSQLBackupToS3.Properties.Settings.Default.S3BucketName,MSSQLBackupToS3.Properties.Settings.Default.S3RegionSystemName, MSSQLBackupToS3.Properties.Settings.Default.DBServerName, MSSQLBackupToS3.Properties.Settings.Default.DBName, MSSQLBackupToS3.Properties.Settings.Default.fullBackupPeriodInMinutes,MSSQLBackupToS3.Properties.Settings.Default.incrementalBackupPeriodInMinutes));
            sc.WhenStarted(s => s.Start());
            sc.WhenStopped(s => s.Stop());
        }
    }
}
