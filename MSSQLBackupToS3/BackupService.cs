﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Diagnostics;
using Amazon.S3;
using Amazon;
using Amazon.S3.Transfer;

namespace MSSQLBackupToS3
{
    class BackupService
    {
        private System.Timers.Timer timer;

        string S3AccessKey;
        string S3SecretAccessKey;
        string bucketName;
        string dbServer;
        string database;
        string regionSystemName;
        int fullBackupPeriodInMinutes;
        int incrementalBackupPeriodInMinutes;
        int numberOfIncrementalBackupsUntilFullBackup;

        internal BackupService(string S3AccessKey, string S3SecretAccessKey, string bucketName,string regionSystemName, string dbServer, string database,int fullBackupPeriodInMinutes, int incrementalBackupPeriodInMinutes)
        {
            this.S3AccessKey = S3AccessKey;
            this.S3SecretAccessKey = S3SecretAccessKey;
            this.bucketName = bucketName;
            this.regionSystemName = regionSystemName;
            this.dbServer = dbServer;
            this.database = database;
            this.fullBackupPeriodInMinutes = fullBackupPeriodInMinutes;
            this.incrementalBackupPeriodInMinutes = incrementalBackupPeriodInMinutes;
            this.numberOfIncrementalBackupsUntilFullBackup = this.fullBackupPeriodInMinutes / this.incrementalBackupPeriodInMinutes;
        }

        private void StartTimer()
        {
            if (this.timer != null) this.timer.Dispose();
            this.timer = new System.Timers.Timer(this.incrementalBackupPeriodInMinutes*60*1000) { AutoReset = false }; // 5 minutos
            this.timer.Elapsed += (o, e) =>
            {
                try
                {
                    this.Backup();
                }
                catch (Exception exp)
                {
                    EventLog.WriteEntry("MSSQLBackupToS3", String.Format("{0} \n {1}",exp.Message,exp.StackTrace), EventLogEntryType.Error);
                    EventLog.WriteEntry("MSSQLBackupToS3", exp.Source);
                }
                finally
                {
                    this.timer.Enabled = true;
                }
            };
            this.timer.Enabled = true;

        }

        internal void Backup()
        {
            Server srv = new Server(this.dbServer);
            string filePath = System.IO.Path.Combine(srv.BackupDirectory,String.Format("{0}.bak",this.database));

            Backup backup = new Backup();

            // specify what you want to backup
            backup.Action = BackupActionType.Database;

            // specify the name of the database
            backup.Database = this.database;

            // specify what kind of devides to use, in this example we are using the File Device
            backup.Devices.AddDevice(@filePath, DeviceType.File);
            backup.BackupSetName = this.database;
            backup.BackupSetDescription = String.Format("Backup from database: {0}",this.database);

            // setting the expiration date
            backup.ExpirationDate = DateTime.Today.AddDays(1);


            // setting incremental backup
            if (this.numberOfIncrementalBackupsUntilFullBackup <= 1)
            {
                backup.Incremental = false;
                //resetting counter;
                this.numberOfIncrementalBackupsUntilFullBackup = this.fullBackupPeriodInMinutes / this.incrementalBackupPeriodInMinutes;               
            }
            else
            {
                backup.Incremental = true;
                this.numberOfIncrementalBackupsUntilFullBackup--;
            }

            // events called to show progress and when the backup finish
            backup.PercentComplete += (sender, args) =>
                {
                    Console.Clear();
                    Console.WriteLine("Percent completed: {0}%.", args.Percent);
                };
            backup.Complete += (sender, args) =>
            {
                Console.WriteLine("Backup completed.");
                Console.WriteLine(args.Error.Message);
                EventLog.WriteEntry("MSSQLBackupToS3", "Database backup completed.", EventLogEntryType.Information);

                //Upload to S3
                string fileName = System.IO.Path.GetFileName(filePath);
                string keyName = String.Format("DBBackups/{0}",fileName);
                EventLog.WriteEntry("MSSQLBackupToS3", String.Format("Uploading database {0} to AWS S3...", this.database), EventLogEntryType.Information);
                Console.WriteLine("Uploading database {0} to AWS S3...", this.database);
                bool uploadResult = this.Upload(keyName, filePath);
                if (uploadResult)
                {
                    EventLog.WriteEntry("MSSQLBackupToS3", String.Format("Uploaded database {0} to AWS S3.", this.database), EventLogEntryType.Information);
                }
                else
                {
                    EventLog.WriteEntry("MSSQLBackupToS3", String.Format("Could not upload database {0} to AWS S3.", this.database), EventLogEntryType.Error);
                }
            };

            backup.LogTruncation = BackupTruncateLogType.Truncate;

            // start backup, this method is asynchronous
            backup.SqlBackup(srv);
        }
        private bool Upload(string keyName, string filepath)
        {
            try
            {
                using (IAmazonS3 client = AWSClientFactory.CreateAmazonS3Client(this.S3AccessKey, this.S3SecretAccessKey, RegionEndpoint.GetBySystemName(this.regionSystemName)))
                {
                    TransferUtility fileTransferUtility = new TransferUtility(client);
                    fileTransferUtility.Upload(filepath, this.bucketName, keyName);
                    return true;
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    EventLog.WriteEntry("MSSQLBackupToS3", "Check the provied AWS Credentials",EventLogEntryType.Error);
                    Trace.WriteLine("Check the provided AWS Credentials.");
                }
                else
                {
                    string message = String.Format("Error occurred. Message:'{0}' when writing an object. Error Code: {1}.Error Type: {2}. Status Error: {3}", amazonS3Exception.Message, amazonS3Exception.ErrorCode, amazonS3Exception.ErrorType, amazonS3Exception.StatusCode);
                    EventLog.WriteEntry("MSSQLBackupToS3", message, EventLogEntryType.Error);
                    Trace.WriteLine(message);
                }
            }
            catch (Exception e)
            {
                string message = String.Format("{0}. {1}", e.Message, e.StackTrace);
                EventLog.WriteEntry("MSSQLBackupToS3", message, EventLogEntryType.Error);
            }
            return false;
        }


        // Run once and then start the timer
        internal bool Start()
        {
            try
            {
                this.Backup();
            }
            catch (Exception exp)
            {
                EventLog.WriteEntry("MSSQLBackupToS3", String.Format("{0} \n {1}", exp.Message, exp.StackTrace), EventLogEntryType.Error);
                EventLog.WriteEntry("MSSQLBackupToS3", exp.Source);
            }
            try
            {
                this.StartTimer();
                EventLog.WriteEntry("MSSQLBackupToS3", "Started service.", EventLogEntryType.Information);
                return true;
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("MSSQLBackupToS3", String.Format("Error while trying to start timer. Message:{0}. StackTrace{1}", e.Message, e.StackTrace), EventLogEntryType.Error);
                return false;
            }
        }

        internal bool Stop()
        {
            {
                try
                {
                    if (this.timer != null)
                        this.timer.Stop();
                    EventLog.WriteEntry("MSSQLBackupToS3", "Stopped service.", EventLogEntryType.Information);
                    return true;
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("MSSQLBackupToS3", e.Message, EventLogEntryType.Error);
                    EventLog.WriteEntry("MSSQLBackupToS3", e.StackTrace, EventLogEntryType.Error);
                    return false;
                }
            }
        }
    }
}
