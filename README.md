# MSSQL Backup To S3

Basic windows service program that periodically makes a database backup and uploads to AWS S3 service.

# Requisites

* SQL Server 2012 Feature Pack - Shared Management Objects
* SQL Server 2012 Feature Pack - SQLSysClrTypes

# Important
The user running MSSQLServer (normally *NT Service\MSSQLSERVER*) must have write permission on backup directory.